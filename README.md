# sample-spring-boot-gcp-project

This project serves as a bootstrap for further projects based on Spring Boot and Google Cloud Platform.
In this case we use Google App Engine and Google Cloud SQL.

To start, you need to: 
1) put your app Id in appId section of pom.xml
2) Enable SQL and SQL Admin API for the project where you use Cloud SQL https://i.stack.imgur.com/7Unj8.jpg
3) go to Google Cloud settings and grant access to service account of your App Engine project to Cloud SQL database (in case database belongs to another project)
4) download credentials file for this account and provide to project (like sample-credentials.json)
5) fill the application.properties file (instance-connection-name you can take from google cloud admin panel) 
6) to run locally, you need to enable cloud sql proxy on your local environment

for more info please contact david.garibov@datatile.eu