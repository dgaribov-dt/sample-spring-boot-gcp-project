package eu.datatile.billing;

import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloAppEngineApplication {
    @GetMapping("/")
    public String hello() {
        return "Hello Spring Boot!";
    }
}